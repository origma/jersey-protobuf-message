package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import au.com.origma.jersey.protobuf.ProtobufMessageWriter;
import tests.AddressBookProtos.Person;

public class ProtobufWriterTest {

	@Test
	public void testIsWriteable() {
		assertTrue("Wrote", new ProtobufMessageWriter().isWriteable(null, Person.class, null, null));
	}

}
