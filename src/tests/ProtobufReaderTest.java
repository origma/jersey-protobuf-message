package tests;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import org.junit.Test;

import au.com.origma.jersey.protobuf.ProtobufMessageReader;
import tests.AddressBookProtos.Person;

public class ProtobufReaderTest {

	@Test
	public void testIsReadable() {
		ProtobufMessageReader reader = new ProtobufMessageReader();
		assertTrue("Protobuf is readable", reader.isReadable(Person.class, Person.class, new Annotation[]{}, new MediaType("","")));
	}

	@Test
	public void testReadFrom() {
		ProtobufMessageReader reader = new ProtobufMessageReader();
		
		Person person = Person.newBuilder()
				.setId(1)
				.setName("James")
				.build();
		InputStream in = new ByteArrayInputStream(person.toByteArray());
		
		try {
			Person rPerson = (Person) reader.readFrom(null, person.getClass(), new Annotation[]{}, new MediaType("",""), null, in);
			assertTrue("Successfully read protobuf! " + rPerson.toString(), rPerson instanceof Person);
		} catch (WebApplicationException | IOException e) {
			e.printStackTrace();
			fail("Unable to read");
		}
	}
	
	public static void main(String[] args){
		new ProtobufReaderTest().testReadFrom();
	}

}
