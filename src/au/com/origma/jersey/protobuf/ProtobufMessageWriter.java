package au.com.origma.jersey.protobuf;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;

import com.google.protobuf.AbstractMessage;

public class ProtobufMessageWriter implements MessageBodyWriter<AbstractMessage>{

	@Override
	public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediatype) {
		Class<?> sc = ((Class<?>) genericType).getSuperclass().getSuperclass();
		return sc == AbstractMessage.class;
	}

	@Override
	public void writeTo(AbstractMessage message, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediatype,
			MultivaluedMap<String, Object> httpHeaders, OutputStream out) throws IOException, WebApplicationException {
		
		out.write(message.toByteArray());
		
	}

}
