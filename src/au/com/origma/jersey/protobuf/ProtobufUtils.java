package au.com.origma.jersey.protobuf;

import javax.ws.rs.core.MediaType;

public class ProtobufUtils {
	
	public static final String APPLICATION_PROTOBUF = "application/protobuf";
	public static final MediaType APPLICATION_PROTOBUF_TYPE = new MediaType("application","protobuf");

}
