package au.com.origma.jersey.protobuf;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;

import com.google.protobuf.AbstractMessage;

/**
 * Parses Protobuf input streams into their correct object
 * @author Ben McLean
 */
public class ProtobufMessageReader implements MessageBodyReader<AbstractMessage>{

	@Override
	public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediatype) {
		Class<?> sc = ((Class<?>) genericType).getSuperclass().getSuperclass();
		return sc == AbstractMessage.class;
	}
	
	@Override
	public AbstractMessage readFrom(Class<AbstractMessage> type, Type genericType, Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, String> httpHeaders, InputStream entityStream) throws IOException, WebApplicationException {
		//Check if the stream can be parsed
		if(!isReadable(type, genericType, annotations, mediaType)){
			return null;
		}
		
		//Cast the type to a message class
		Class<? extends AbstractMessage> c = (Class<? extends AbstractMessage>) genericType;
		try {
			//Get the parse method of the class
			Method m = c.getDeclaredMethod("parseFrom", InputStream.class);
			//Invoke the message
			return (AbstractMessage) m.invoke(null, entityStream);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}
}
